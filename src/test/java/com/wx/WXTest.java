package com.wx;

import com.baidu.aip.ocr.AipOcr;
import com.wx.model.button.*;
import com.wx.model.templatemessage.First;
import com.wx.model.templatemessage.Keyworld;
import com.wx.model.templatemessage.Remark;
import com.wx.model.templatemessage.WxTemplateMessage;
import com.wx.model.weather.*;
import com.wx.repository.UserRepository;
import com.wx.service.Impl.WXServiceImpl;
import com.wx.service.UserService;
import com.wx.util.CommonUtil;
import com.wx.util.CreateMenu;
import com.wx.util.RedisUtils;
import net.sf.json.JSONObject;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 12:55 下午
 * @desc ：
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WXTest {

    @Autowired
    public RedisUtils redisUtil;
    @Autowired
    public WXServiceImpl wxService;
    @Autowired
    UserService userService;
    @Autowired
    CreateMenu menu;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RedisUtils redisUtils;

    public static final String APP_ID = "22800901";
    public static final String API_KEY = "2D0gCVQP3sRXf8qKgGdjgUCs";
    public static final String SECRET_KEY = "UXL0pNYD2OVVWGqHURGF0fZO357kmXBa";
    public static final String LJYopenID = "oCV_T6GUj67UKShxzFXbExpFYcYA";//刘菁源
    public static final String LSHopenID = "oCV_T6IutImdnwuD658EyIMM_goU";//刘世豪
    public static final String WYTopenID = "oCV_T6FlZ4U0EEhNlVi51udzQI8U";//王艺曈
    public static final String WJCopenID = "oCV_T6HEG-hy1-cZrp1aaK7NwmuI";//王俊超

    /**
     * 40003   不合法的 OpenID ，请开发者确认 OpenID （该用户）是否已关注公众号，或是否是其他公众号的 OpenID
     */

    @Test
    public void test2(){
        Button button = new Button();
        button.getButton().add(new ClickButten("一级菜单_点击","1"));
        button.getButton().add(new ViewButton("一级跳转","http://baidu.com"));
        SubButton subButton = new SubButton("子菜单");
        subButton.getSub_button().add(new PhotoAlbumButton("系统拍照发图","pic_sysphoto","rselfmenu_1_0"));
        subButton.getSub_button().add(new ClickButten("二级菜单_点击","2"));
        subButton.getSub_button().add(new ViewButton("二级跳转","http://news.163.com"));
        button.getButton().add(subButton);
        JSONObject jsonObject = JSONObject.fromObject(button);
        System.out.println(jsonObject);

    }

    /**
     * 清空微信公众号的菜单
     */
    @Test
    public void updateMenu(){
        menu.delMenu();
        menu.createMenu();
        menu.findMenu();
    }

    @Test
    public void test4() throws JSONException {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("language_type", "CHN_ENG");
        options.put("detect_direction", "true");
        options.put("detect_language", "true");
        options.put("probability", "true");

//        // 参数为本地路径
//        String image = "test.jpg";
//        JSONObject res = client.basicGeneral(image, options);
//        System.out.println(res.toString(2));
//
//        // 参数为二进制数组
//        byte[] file = readFile("test.jpg");
//        res = client.basicGeneral(file, options);
//        System.out.println(res.toString(2));

        // 通用文字识别, 图片参数为远程url图片
        String url = "http://mmbiz.qpic.cn/mmbiz_jpg/G6FDfpc5D6lBLvic2IothARyvfJTW1DNee1BpuJcA423PCQa1C3sOczeg6K0vCkicrRe2p9mcer4ERwSLf8ocr8Q/0";
        org.json.JSONObject res = client.basicGeneralUrl(url, options);
        System.out.println(res.toString(2));
    }

    @Test
    public void test5(){
        String Url = "https://jisuweather.api.bdymkt.com/weather/query?city=上海";
        com.alibaba.fastjson.JSONObject get = CommonUtil.httpsRequest(Url, "GET", null);
        System.out.println(get.toString());
    }
    @Test
    public void test6(){
        String Url = "https://jisuweather.api.bdymkt.com/weather/query?city=上海";
//        String Url = "http://api.map.baidu.com/telematics/v3/weather?location=上海&output=xml&ak=72hu6IhfrWxUXi67NuKdMuuanH2bPVBf";
        StringBuffer sb = null;
        try {
//            URL url = new URL(Url);
//            URLConnection conn = url.openConnection();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));//转码。
//            String line = null;
//            while ((line = reader.readLine()) != null)
//                strBuf.append(line + " ");
//            reader.close();
//            建立连接
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setRequestMethod("GET");
            //设置请求头
            httpUrlConn.setRequestProperty("X-Bce-Signature","AppCode/0a7f170702e341f6b3cbd8a4e8274cb9");
            //httpUrlConn.connect();     可要    可不要
//            httpUrlConn.connect();
            // 获取输入流
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);

    }
    @Test
    public void test7(){
        String str = "{\"status\":0,\"msg\":\"ok\",\"result\":{\"city\":\"上海\",\"cityid\":24,\"citycode\":\"101020100\",\"date\":\"2020-10-11\",\"week\":\"星期日\",\"weather\":\"多云\",\"temp\":\"23\",\"temphigh\":\"24\",\"templow\":\"18\",\"img\":\"1\",\"humidity\":\"50\",\"pressure\":\"1014\",\"windspeed\":\"2.9\",\"winddirect\":\"静风\",\"windpower\":\"0级\",\"updatetime\":\"2020-10-11 11:35:00\",\"index\":[{\"iname\":\"空调指数\",\"ivalue\":\"较少开启\",\"detail\":\"您将感到很舒适，一般不需要开启空调。\"},{\"iname\":\"运动指数\",\"ivalue\":\"适宜\",\"detail\":\"天气较好，赶快投身大自然参与户外运动，尽情感受运动的快乐吧。\"},{\"iname\":\"紫外线指数\",\"ivalue\":\"弱\",\"detail\":\"紫外线强度较弱，建议出门前涂擦SPF在12-15之间、PA+的防晒护肤品。\"},{\"iname\":\"感冒指数\",\"ivalue\":\"少发\",\"detail\":\"各项气象条件适宜，无明显降温过程，发生感冒机率较低。\"},{\"iname\":\"洗车指数\",\"ivalue\":\"较适宜\",\"detail\":\"较适宜洗车，未来一天无雨，风力较小，擦洗一新的汽车至少能保持一天。\"},{\"iname\":\"空气污染扩散指数\",\"ivalue\":\"中\",\"detail\":\"气象条件对空气污染物稀释、扩散和清除无明显影响，易感人群应适当减少室外活动时间。\"},{\"iname\":\"穿衣指数\",\"ivalue\":\"舒适\",\"detail\":\"建议着长袖T恤、衬衫加单裤等服装。年老体弱者宜着针织长袖衬衫、马甲和长裤。\"}],\"aqi\":{\"so2\":\"8\",\"so224\":\"7\",\"no2\":\"27\",\"no224\":\"38\",\"co\":\"0.660\",\"co24\":\"0.560\",\"o3\":\"104\",\"o38\":\"51\",\"o324\":\"58\",\"pm10\":\"35\",\"pm1024\":\"36\",\"pm2_5\":\"21\",\"pm2_524\":\"18\",\"iso2\":\"3\",\"ino2\":\"14\",\"ico\":\"7\",\"io3\":\"33\",\"io38\":\"26\",\"ipm10\":\"35\",\"ipm2_5\":\"31\",\"aqi\":\"35\",\"primarypollutant\":\"PM10\",\"quality\":\"优\",\"timepoint\":\"2020-10-11 11:00:00\",\"aqiinfo\":{\"level\":\"一级\",\"color\":\"#00e400\",\"affect\":\"空气质量令人满意，基本无空气污染\",\"measure\":\"各类人群可正常活动\"}},\"daily\":[{\"date\":\"2020-10-11\",\"week\":\"星期日\",\"sunrise\":\"05:53\",\"sunset\":\"17:28\",\"night\":{\"weather\":\"晴\",\"templow\":\"18\",\"img\":\"0\",\"winddirect\":\"南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"多云\",\"temphigh\":\"24\",\"img\":\"1\",\"winddirect\":\"南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-12\",\"week\":\"星期一\",\"sunrise\":\"05:54\",\"sunset\":\"17:27\",\"night\":{\"weather\":\"多云\",\"templow\":\"19\",\"img\":\"1\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"多云\",\"temphigh\":\"25\",\"img\":\"1\",\"winddirect\":\"东南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-13\",\"week\":\"星期二\",\"sunrise\":\"05:55\",\"sunset\":\"17:25\",\"night\":{\"weather\":\"多云\",\"templow\":\"18\",\"img\":\"1\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"多云\",\"temphigh\":\"24\",\"img\":\"1\",\"winddirect\":\"南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-14\",\"week\":\"星期三\",\"sunrise\":\"05:55\",\"sunset\":\"17:24\",\"night\":{\"weather\":\"阴\",\"templow\":\"18\",\"img\":\"2\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"多云\",\"temphigh\":\"23\",\"img\":\"1\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-15\",\"week\":\"星期四\",\"sunrise\":\"05:56\",\"sunset\":\"17:23\",\"night\":{\"weather\":\"小雨\",\"templow\":\"17\",\"img\":\"7\",\"winddirect\":\"南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"阴\",\"temphigh\":\"21\",\"img\":\"2\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-16\",\"week\":\"星期五\",\"sunrise\":\"05:57\",\"sunset\":\"17:22\",\"night\":{\"weather\":\"阴\",\"templow\":\"16\",\"img\":\"2\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"小雨\",\"temphigh\":\"21\",\"img\":\"7\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"}},{\"date\":\"2020-10-17\",\"week\":\"星期六\",\"sunrise\":\"05:57\",\"sunset\":\"17:21\",\"night\":{\"weather\":\"多云\",\"templow\":\"18\",\"img\":\"1\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"},\"day\":{\"weather\":\"阴\",\"temphigh\":\"22\",\"img\":\"2\",\"winddirect\":\"西南风\",\"windpower\":\"微风\"}}],\"hourly\":[{\"time\":\"11:00\",\"weather\":\"多云\",\"temp\":\"24\",\"img\":\"1\"},{\"time\":\"12:00\",\"weather\":\"多云\",\"temp\":\"24\",\"img\":\"1\"},{\"time\":\"13:00\",\"weather\":\"阴\",\"temp\":\"24\",\"img\":\"2\"},{\"time\":\"14:00\",\"weather\":\"阴\",\"temp\":\"24\",\"img\":\"2\"},{\"time\":\"15:00\",\"weather\":\"阴\",\"temp\":\"24\",\"img\":\"2\"},{\"time\":\"16:00\",\"weather\":\"阴\",\"temp\":\"24\",\"img\":\"2\"},{\"time\":\"17:00\",\"weather\":\"阴\",\"temp\":\"23\",\"img\":\"2\"},{\"time\":\"18:00\",\"weather\":\"多云\",\"temp\":\"23\",\"img\":\"1\"},{\"time\":\"19:00\",\"weather\":\"多云\",\"temp\":\"22\",\"img\":\"1\"},{\"time\":\"20:00\",\"weather\":\"晴\",\"temp\":\"21\",\"img\":\"0\"},{\"time\":\"21:00\",\"weather\":\"晴\",\"temp\":\"20\",\"img\":\"0\"},{\"time\":\"22:00\",\"weather\":\"晴\",\"temp\":\"20\",\"img\":\"0\"},{\"time\":\"23:00\",\"weather\":\"晴\",\"temp\":\"20\",\"img\":\"0\"},{\"time\":\"0:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"1:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"2:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"3:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"4:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"5:00\",\"weather\":\"晴\",\"temp\":\"18\",\"img\":\"0\"},{\"time\":\"6:00\",\"weather\":\"晴\",\"temp\":\"19\",\"img\":\"0\"},{\"time\":\"7:00\",\"weather\":\"晴\",\"temp\":\"20\",\"img\":\"0\"},{\"time\":\"8:00\",\"weather\":\"晴\",\"temp\":\"20\",\"img\":\"0\"},{\"time\":\"9:00\",\"weather\":\"多云\",\"temp\":\"22\",\"img\":\"1\"},{\"time\":\"10:00\",\"weather\":\"多云\",\"temp\":\"24\",\"img\":\"1\"}]}}\n";
        WeatherResponse response = com.alibaba.fastjson.JSONObject.parseObject(str, WeatherResponse.class);
        System.out.println("weatherResponse:"+response);


        WeatherResult result = com.alibaba.fastjson.JSONObject.parseObject(response.getResult(), WeatherResult.class);
        System.out.println("weatherResult:"+result);


        List<WeatherIndex> index = com.alibaba.fastjson.JSONObject.parseArray(result.getIndex(), WeatherIndex.class);
        StringBuffer sb1 = new StringBuffer();
        for (WeatherIndex weatherIndex : index) {
            sb1.append(weatherIndex.getIname());
            sb1.append(":");
            sb1.append(weatherIndex.getIvalue()+"，"+weatherIndex.getDetail());
            sb1.append("\r\n");
        }
        System.out.println("sb1:"+sb1);


        WeatherAQI aqi = com.alibaba.fastjson.JSONObject.parseObject(result.getAqi(), WeatherAQI.class);
        WeatherAQIInfo aqiInfo = com.alibaba.fastjson.JSONObject.parseObject(aqi.getAqiInfo(), WeatherAQIInfo.class);
        System.out.println("aqi:"+aqi);
        System.out.println("aqiInfo:"+aqiInfo);
        String aqiStr = "空气等级："+aqiInfo.getLevel()+"，"+aqi.getQuality()+"，"+aqiInfo.getMeasure()+"，"+aqiInfo.getAffect();
        System.out.println("空气质量信息："+aqiStr);
        List<WeatherDaily> dailies = com.alibaba.fastjson.JSONObject.parseArray(result.getDaily(), WeatherDaily.class);
        StringBuffer sb2 = new StringBuffer();
        for (WeatherDaily daily : dailies) {
            DayAndNight day = com.alibaba.fastjson.JSONObject.parseObject(daily.getDay(), DayAndNight.class);
            DayAndNight night = com.alibaba.fastjson.JSONObject.parseObject(daily.getNight(), DayAndNight.class);
            sb2.append(daily.getDate()+","+daily.getWeek()+" , 白天："+day.getWeather()+",最高温度："+day.getTemphigh()+","+day.getWinddirect()+","+day.getWindpower()+"; 夜间:"+night.getWeather()+",最高温度："+night.getTemplow()+","+night.getWinddirect()+","+night.getWindpower());
            sb2.append("\r\n");
        }
        System.out.println("sb2:"+sb2);

    }

    @Test
    public void test8(){
        String info = "你好啊";
        String jsonStrInfo = com.alibaba.fastjson.JSONObject.toJSONString(info);
        System.out.println("jsonStrInfo:"+jsonStrInfo);
        String Url = "https://ltjqr.api.bdymkt.com/ltjqr";
        StringBuffer sb = null;
        try {
//            建立连接
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod("POST");
            //设置请求头
            httpUrlConn.setRequestProperty("X-Bce-Signature","AppCode/0a7f170702e341f6b3cbd8a4e8274cb9");
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");//设置参数类型是json格式
            //httpUrlConn.connect();     可要    可不要
//            httpUrlConn.connect();
            String body = "{info:"+info+"}";
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
            writer.write(body);
            writer.close();
            // 获取输入流
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
    }

    /**
     * 设置所属行业
     * http请求方式: POST https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN
     * access_token	是	接口调用凭证
     * industry_id1	是	公众号模板消息所属行业编号
     * industry_id2	是	公众号模板消息所属行业编号
     *
     * {
     *     "industry_id1":"1",
     *     "industry_id2":"4"
     * }
     */
    @Test
    public void test9(){
        String redisToken = wxService.getRedisToken();
        String Url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN";
        String access_token = Url.replace("ACCESS_TOKEN", redisToken);
        String data="{\n" +
                "    \"industry_id1\":\"1\",\n" +
                "    \"industry_id2\":\"5\"\n" +
                "}";
        StringBuffer sb = null;
        try {
//            建立连接
            URL url = new URL(access_token);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod("POST");
            //设置请求头   //设置参数类型是json格式
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            //httpUrlConn.connect();     可要    可不要
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
            writer.write(data);
            writer.close();
            // 获取输入流
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
    }

    /**
     * 获取设置的行业信息
     * 获取帐号设置的行业信息。可登录微信公众平台，在公众号后台中查看行业信息。为方便第三方开发者，提供通过接口调用的方式来获取帐号所设置的行业信息，具体如下:
     * 接口调用请求说明
     * http请求方式：GET https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
     */
    @Test
    public void test10(){
        String redisToken = wxService.getRedisToken();
        String Url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN";
        String url = Url.replace("ACCESS_TOKEN", redisToken);
        httpClient(url, "Get", null);

    }






    public String httpClient(String Url,String RequestMethod,String data){
        StringBuffer sb = null;
        try {
//            建立连接
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            String s = RequestMethod.toUpperCase();
            httpUrlConn.setRequestMethod(s);
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            if ("GET".equalsIgnoreCase(RequestMethod)){
                httpUrlConn.connect();
            }else {
                //设置请求头   //设置参数类型是json格式
                //httpUrlConn.connect();     可要    可不要
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
                writer.write(data);
                writer.close();
                // 获取输入流

            }
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
        return sb.toString();
    }

    /**
     * 发送模板消息
     * 接口调用请求说明
     * http请求方式: POST https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
     * 参数：
     *
     *  {
     *            "touser":"oCV_T6IutImdnwuD658EyIMM_goU",
     *            "template_id":"JIMtRhfmeks-H3f0dn_P0wcnBnqiTPIxl7uahiZqvHI",
     *            "url":"http://weixin.qq.com/download",
     *            "data":{
     *                    "first": {
     *                        "value":"恭喜您订购成功！",
     *                        "color":"#173177"
     *                    },
     *                    "keyword1":{
     *                        "value":"iPhone 12 Pro Max 256G 暗夜绿",
     *                        "color":"#173177"
     *                    },
     *                    "keyword2": {
     *                        "value":"999$",
     *                        "color":"#173177"
     *                    },
     *                    "keyword3": {
     *                        "value":"2014年9月22日",
     *                        "color":"#173177"
     *                    },
     *                    "remark":{
     *                        "value":"欢迎再次购买！",
     *                        "color":"#173177"
     *                    }
     *            }
     *        }
     *        40003	不合法的 OpenID ，请开发者确认 OpenID （该用户）是否已关注公众号，或是否是其他公众号的 OpenID
     */
    @Test
    public void test11(){
//        List<User> users = userRepository.findUserByFlagIs0();
        String redisToken = wxService.getRedisToken();
//        for (User user : users) {
//            String openID = user.getOpenID();
//            String openID = "oCV_T6GUj67UKShxzFXbExpFYcYA";//刘菁源
            String openID = "oCV_T6IutImdnwuD658EyIMM_goU";//刘世豪
//        String openID = "oCV_T6FlZ4U0EEhNlVi51udzQI8U";//王艺曈
//        String openID = "oCV_T6HEG-hy1-cZrp1aaK7NwmuI";//王俊超

        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss"));
        String serialNumber = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))+(Math.random()*100000+"").substring(0,4);
//            First first = new First("尊敬的 "+user.getName()+" 用户，恭喜您订购成功！","#173177");
        First first = new First("尊敬的  用户，恭喜您订购成功！","#173177");
        //构造data参数对象
        //订单商品
        Keyworld keyword1 = new Keyworld("iPhone 12 Pro Max 256G 海军蓝","#173177");
        //订单编号
        Keyworld keyword2 = new Keyworld("202010140315325205","#173177");
        //支付金额
        Keyworld keyword3 = new Keyworld("1299.00$","#173177");
        //支付时间
        Keyworld keyword4 = new Keyworld("2020年10月14日 03:15:32","#173177");
        Remark remark = new Remark("祝您生活愉快！","#173177");
        //将data参数对象 转化成json
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword11 = JSONObject.fromObject(keyword1);
        JSONObject keyword21 = JSONObject.fromObject(keyword2);
        JSONObject keyword31 = JSONObject.fromObject(keyword3);
        JSONObject keyword41 = JSONObject.fromObject(keyword4);
        JSONObject remark1 = JSONObject.fromObject(remark);
        //构造进  data参数
        JSONObject templateData2 = new JSONObject();
        templateData2.put("first",first1);
        templateData2.put("keyword1",keyword11);
        templateData2.put("keyword2",keyword21);
        templateData2.put("keyword3",keyword31);
        templateData2.put("keyword4",keyword41);
        templateData2.put("remark",remark1);
        //构造 请求参数对象
        WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(openID, "JIMtRhfmeks-H3f0dn_P0wcnBnqiTPIxl7uahiZqvHI", "https://www.apple.com/cn/",null);

        //将请求参数对象  转化成 JSON
        JSONObject msgData = JSONObject.fromObject(wxTemplateMessage1);
        msgData.put("data",templateData2.toString());
        System.out.println(msgData);
        httpClient(access_token, "POST", msgData.toString());
//    }
}

    @Test
    public void  test12(){
        String wx_access_token = (String)redisUtils.get("WX_ACCESS_Token");
        long wx_access_token_expire = (long) redisUtils.getExpire("WX_ACCESS_Token");
        System.out.println(wx_access_token+":"+wx_access_token_expire);
        redisUtils.del("WX_ACCESS_Token");

        redisUtils.del("oCV_T6IutImdnwuD658EyIMM_goU-isSendWeather");
        redisUtils.del("oCV_T6FlZ4U0EEhNlVi51udzQI8U-isSendWeather");
        System.out.println(redisUtils.get("oCV_T6G8VjCPZINCN3o0vHIYy2YQ-isSendWeather"));
        System.out.println(redisUtils.getExpire("oCV_T6G8VjCPZINCN3o0vHIYy2YQ-isSendWeather"));
        redisUtils.del("oCV_T6G8VjCPZINCN3o0vHIYy2YQ-isSendWeather");
    }

    @Test
    public void test13(){
        String days = "";
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
//        List<User> users = repository.findUserByFlagIs0();
//        for (User user : users) {
//            if (org.springframework.util.StringUtils.isEmpty(user.getDowndate())){
//                continue;
//            }
            //发工资日期
//            int downdateInt = Integer.parseInt(user.getDowndate());
            int downdateInt = 10;
            LocalDateTime now = LocalDateTime.now();
            //当天是几号
            String todayDD = now.format(DateTimeFormatter.ofPattern("dd"));
            //当月
            String todayMM = now.format(DateTimeFormatter.ofPattern("MM"));
            //下个月
            String nextMM = now.plusMonths(1).format(DateTimeFormatter.ofPattern("MM"));
            //当年
            String todayyyyy = now.format(DateTimeFormatter.ofPattern("yyyy"));

            //判断当天是否已经过了发工资的日期
            int todayInt = Integer.parseInt(todayDD);
            if(downdateInt<todayInt){
                // 说明本月工资已经发过了  需要和下个月日期比较
                Duration between = Duration.between(now,LocalDateTime.of(Integer.parseInt(todayyyyy),Integer.parseInt(nextMM),downdateInt,0,0,0));
                long l = between.toDays();
                days = Long.toString(l);
                System.out.println();
            }else {
                //否则说明  发工资日期 大于 今天 本月工资还没有发
                days = Integer.toString( downdateInt- todayInt);
            }
            String yyyyMMdd = now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
            DayOfWeek dayOfWeek = now.getDayOfWeek();
            String week = "";
            switch (dayOfWeek){
                case MONDAY:
                    week = "星期一";
                    break;
                case TUESDAY:
                    week = "星期二";
                    break;
                case WEDNESDAY:
                    week = "星期三";
                    break;
                case THURSDAY:
                    week = "星期四";
                    break;
                case FRIDAY:
                    week = "星期五";
                    break;
                case SUNDAY:
                    week = "星期六";
                    break;
                default:
                    week = "星期天";
                    break;
            }
//            First first = new First(user.getName(),"#173177");
        First first = new First("刘世豪","#173177");
        Keyworld keyworld0 = new Keyworld(yyyyMMdd+" "+week,"#173177");
        Keyworld keyworld1 = new Keyworld("发工资","#173177");
        Keyworld keyworld2 = new Keyworld(days,"#173177");
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject templateData = new JSONObject();
        templateData.put("first",first1);
        templateData.put("keyword0",keyword01);
        templateData.put("keyword1",keyword11);
        templateData.put("keyword2",keyword21);
        WxTemplateMessage wxTemplateMessage = new WxTemplateMessage(LSHopenID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null,null);
            JSONObject msgData = JSONObject.fromObject(wxTemplateMessage);
            msgData.put("data",templateData.toString());
            System.out.println(msgData);
            httpClient(access_token, "POST", msgData.toString());
    }


}
