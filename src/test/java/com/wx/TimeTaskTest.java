package com.wx;

import com.thoughtworks.xstream.XStream;
import com.wx.model.UploadYJSC;
import com.wx.model.User;
import com.wx.model.message.Music;
import com.wx.model.message.MusicMessage;
import com.wx.model.templatemessage.First;
import com.wx.model.templatemessage.Keyworld;
import com.wx.model.templatemessage.Remark;
import com.wx.model.templatemessage.WxTemplateMessage;
import com.wx.repository.UserRepository;
import com.wx.service.Impl.WXServiceImpl;
import com.wx.util.CommonUtil;
import com.wx.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/14 3:40 下午
 * @desc ：
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class TimeTaskTest {
    @Autowired
    public RedisUtils redisUtil;
    @Autowired
    public WXServiceImpl wxService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommonUtil commonUtil;
    public static final String LJYopenID = "oCV_T6GUj67UKShxzFXbExpFYcYA";//刘菁源
    public static final String LSHopenID = "oCV_T6IutImdnwuD658EyIMM_goU";//刘世豪
    public static final String WYTopenID = "oCV_T6FlZ4U0EEhNlVi51udzQI8U";//王艺曈
    public static final String WJCopenID = "oCV_T6HEG-hy1-cZrp1aaK7NwmuI";//王俊超
    public static final String LMZ = "刘明珠";
    public static final String MESSAGE = " 宝贝生日 ";
    public static final String LMZOPEN = "oCV_T6G8VjCPZINCN3o0vHIYy2YQ";

    /**
     * 刘明珠离职提醒
     */
    @Test
    public void test1() {
        LocalDateTime of = LocalDateTime.of(2020, 12, 31, 00, 0, 0);
        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(now, of);
        long l = between.toDays();
        String days = Long.toString(l);
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        String week = "";
        switch (dayOfWeek) {
            case MONDAY:
                week = "星期一";
                break;
            case TUESDAY:
                week = "星期二";
                break;
            case WEDNESDAY:
                week = "星期三";
                break;
            case THURSDAY:
                week = "星期四";
                break;
            case FRIDAY:
                week = "星期五";
                break;
            case SUNDAY:
                week = "星期六";
                break;
            default:
                week = "星期天";
                break;
        }
        String yyyyMMdd = now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        First first = new First(LMZ, "#173177");
        Keyworld keyworld0 = new Keyworld(yyyyMMdd + " " + week, "#173177");
        Keyworld keyworld1 = new Keyworld(MESSAGE, "#173177");
        Keyworld keyworld2 = new Keyworld(days, "#173177");
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject templateData = new JSONObject();
        templateData.put("first", first1);
        templateData.put("keyword0", keyword01);
        templateData.put("keyword1", keyword11);
        templateData.put("keyword2", keyword21);
        WxTemplateMessage wxTemplateMessage = new WxTemplateMessage(LMZOPEN, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null, null);
        JSONObject msgData = JSONObject.fromObject(wxTemplateMessage);
        msgData.put("data", templateData.toString());
        System.out.println(msgData);
        httpClient(access_token, "POST", msgData.toString());

    }

    /**
     * 王艺曈生日提醒
     */
    @Test
    public void test2() {
        LocalDateTime of = LocalDateTime.of(2020, 12, 10, 00, 0, 0);
        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(now, of);
        long l = between.toDays();
        String days = Long.toString(l);
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        String week = "";
        switch (dayOfWeek) {
            case MONDAY:
                week = "星期一";
                break;
            case TUESDAY:
                week = "星期二";
                break;
            case WEDNESDAY:
                week = "星期三";
                break;
            case THURSDAY:
                week = "星期四";
                break;
            case FRIDAY:
                week = "星期五";
                break;
            case SUNDAY:
                week = "星期六";
                break;
            default:
                week = "星期天";
                break;
        }
        String yyyyMMdd = now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        First first = new First(" 宝贝 ", "#173177");
        Keyworld keyworld0 = new Keyworld(yyyyMMdd + " " + week, "#173177");
        Keyworld keyworld1 = new Keyworld(MESSAGE, "#173177");
        Keyworld keyworld2 = new Keyworld(days, "#173177");
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject templateData = new JSONObject();
        templateData.put("first", first1);
        templateData.put("keyword0", keyword01);
        templateData.put("keyword1", keyword11);
        templateData.put("keyword2", keyword21);
        WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(LSHopenID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null, null);
        WxTemplateMessage wxTemplateMessage2 = new WxTemplateMessage(WYTopenID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null, null);
        JSONObject msgData1 = JSONObject.fromObject(wxTemplateMessage1);
        JSONObject msgData2 = JSONObject.fromObject(wxTemplateMessage2);
        msgData1.put("data", templateData.toString());
        msgData2.put("data", templateData.toString());
        System.out.println(msgData1);
        httpClient(access_token, "POST", msgData1.toString());
        httpClient(access_token, "POST", msgData2.toString());

    }

    /**
     * 发送模板消息  瑞幸咖啡
     */
    @Test
    public void test4() {
        Keyworld keyworld0 = new Keyworld("充1赠1狂欢倒计时\r\n", "");
        Keyworld keyworld1 = new Keyworld("10月12日-10月19日\r\n", "");
        Keyworld keyworld2 = new Keyworld("瑞幸咖啡\r\n", "");
        Remark remark = new Remark("         最后3天！全场任意饮品【充1赠1】，5折喝\r\n                     人气爆款厚乳拿铁、精粹奥瑞白！马上充>>>", "#FF2D2D");
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject remark1 = JSONObject.fromObject(remark);
        JSONObject templateData = new JSONObject();
        templateData.put("keyword0", keyword01);
        templateData.put("keyword1", keyword11);
        templateData.put("keyword2", keyword21);
        templateData.put("remark", remark1);
        WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(LSHopenID, "8jFQtuGEQiuJyUR2jrZ-aa4XBGGAf2ykNJjrBOWa4WM", null, null);
        JSONObject msgData1 = JSONObject.fromObject(wxTemplateMessage1);
        msgData1.put("data", templateData.toString());
    }

    @Test
    public void test5() {
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        List<User> user = userRepository.findUserByFlag();
        for (User user1 : user) {
            Keyworld keyworld0 = new Keyworld("充1赠1狂欢倒计时\r\n", "");
            Keyworld keyworld1 = new Keyworld("10月12日-10月19日\r\n", "");
            Keyworld keyworld2 = new Keyworld("瑞幸咖啡\r\n", "");
            Remark remark = new Remark("         最后3天！全场任意饮品【充1赠1】，5折喝\r\n                     人气爆款厚乳拿铁、精粹奥瑞白！马上充>>>", "#FF2D2D");
            JSONObject keyword01 = JSONObject.fromObject(keyworld0);
            JSONObject keyword11 = JSONObject.fromObject(keyworld1);
            JSONObject keyword21 = JSONObject.fromObject(keyworld2);
            JSONObject remark1 = JSONObject.fromObject(remark);
            JSONObject templateData = new JSONObject();
            templateData.put("keyword0", keyword01);
            templateData.put("keyword1", keyword11);
            templateData.put("keyword2", keyword21);
            templateData.put("remark", remark1);
            WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(user1.getOpenID(), "8jFQtuGEQiuJyUR2jrZ-aa4XBGGAf2ykNJjrBOWa4WM", null, null);
            JSONObject msgData1 = JSONObject.fromObject(wxTemplateMessage1);
            msgData1.put("data", templateData.toString());
            httpClient(access_token, "POST", msgData1.toString());
        }
    }

    public String httpClient(String Url, String RequestMethod, String data) {
        StringBuffer sb = null;
        try {
//            建立连接
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            String s = RequestMethod.toUpperCase();
            httpUrlConn.setRequestMethod(s);
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            if ("GET".equalsIgnoreCase(RequestMethod)) {
                httpUrlConn.connect();
            } else {
                //设置请求头   //设置参数类型是json格式
                //httpUrlConn.connect();     可要    可不要
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
                writer.write(data);
                writer.close();
                // 获取输入流

            }
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
        return sb.toString();
    }

    public static final String ITPK_Api_Key = "d3a6d81922f222a3b6c05d0b81c66e8b";
    public static final String ITPK_Api_Secret = "liushihao123";

    @Test
    public void test7() {
        String param = "财神爷灵签";
        String url = "http://i.itpk.cn/api.php?limit=2&api_key=d3a6d81922f222a3b6c05d0b81c66e8b&api_secret=liushihao123&question=" + param;
//        String replace = url.replace("ApiKey", ITPK_Api_Key).replace("ApiSecret", ITPK_Api_Secret);
        httpClient(url, "GET", null);
//        System.out.println(replace);

    }

    /**
     * 上传临时素材
     * http请求方式：POST/FORM，
     * 使用https
     * https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
     *
     * @param path
     * @param type
     */
    public String uploadTemp(String path, String type) {
        String redisToken = wxService.getRedisToken();
        //地址https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
        String url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
        String replaceUrl = url.replace("ACCESS_TOKEN", redisToken).replace("TYPE", type);
        File file = new File(path);
        StringBuilder resp = new StringBuilder();
        try {
            URL urlObj = new URL(replaceUrl);
            HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
            conn.setRequestMethod("POST");//以POST方式提交表单
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", "UTF-8");
            //数据边界
            String boundary = "----------" + System.currentTimeMillis();
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            //获取输出流
            OutputStream out = conn.getOutputStream();
            //创建文件输入流
            FileInputStream fis = new FileInputStream(file);
            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append(boundary);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
            sb.append("Content-Type: applicatin/octet-stream\r\n\r\n");
            out.write(sb.toString().getBytes());
            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }

            String foot = "\r\n--" + boundary + "--\r\n";
            out.write(foot.getBytes());
            out.flush();
            out.close();

            //读取数据
            InputStream in = conn.getInputStream();

            while ((len = in.read(bytes)) != -1) {
                resp.append(new String(bytes, 0, len));
            }
            fis.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp.toString();
    }

    /**
     * 上传临时文件方法
     *
     * @param filePath
     * @param type
     * @return
     * @throws Exception
     */
    public String uploadPic(String filePath, String type) throws Exception {
        String redisToken = wxService.getRedisToken();
        //地址https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
        String urlStr = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
        String replaceUrl = urlStr.replace("ACCESS_TOKEN", redisToken).replace("TYPE", type);
        //返回结果
        String result = null;
        File file = new File(filePath);
        if (!file.exists() || !file.isFile()) {
            throw new IOException("文件不存在");
        }
        URL url = new URL(replaceUrl);
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");//以POST方式提交表单
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);//POST方式不能使用缓存
        //设置请求头信息
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        //设置边界
        String BOUNDARY = "----------" + System.currentTimeMillis();
        conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        //请求正文信息
        //第一部分
        StringBuilder sb = new StringBuilder();
        sb.append("--");//必须多两条道
        sb.append(BOUNDARY);
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data;name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
        sb.append("Content-Type:application/octet-stream\r\n\r\n");
        System.out.println("sb:" + sb);

        //获得输出流
        OutputStream out = new DataOutputStream(conn.getOutputStream());
        //输出表头
        out.write(sb.toString().getBytes("UTF-8"));
        //文件正文部分
        //把文件以流的方式 推送道URL中
        DataInputStream din = new DataInputStream(new FileInputStream(file));
        int bytes = 0;
        byte[] buffer = new byte[1024];
        while ((bytes = din.read(buffer)) != -1) {
            out.write(buffer, 0, bytes);
        }
        din.close();
        //结尾部分
        byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("UTF-8");//定义数据最后分割线
        out.write(foot);
        out.flush();
        out.close();
        if (HttpsURLConnection.HTTP_OK == conn.getResponseCode()) {

            StringBuffer strbuffer = null;
            BufferedReader reader = null;
            try {
                strbuffer = new StringBuffer();
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String lineString = null;
                while ((lineString = reader.readLine()) != null) {
                    strbuffer.append(lineString);
                }
                if (result == null) {
                    result = strbuffer.toString();
                    System.out.println("result:" + result);
                }
            } catch (IOException e) {
                System.out.println("发送POST请求出现异常！" + e);
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
        }
        return result;
    }

    /**
     * 获得临时素材   下载文件  只要得到输入流，就可以从流中读出数据
     */
    //TODO 字符乱码问题： 因为响应是图片格式   所以并不是字符编码问题
    @Test
    public void test8() throws Exception {
        String redisToken = wxService.getRedisToken();
        String urlStr = "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
        String replaceUrl = urlStr.replace("ACCESS_TOKEN", redisToken).replace("MEDIA_ID", "FtA17e_zIAqlFyfOZ-tpkbhPmZFQ-EIKGef4NwwPfNaPqGARTs4eX7yrX8gAV5Mv");
        System.out.println(replaceUrl);
        try {
            URL url = new URL(replaceUrl);
            //得到connection对象。
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //设置请求方式
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
//            connection.setRequestProperty("Content-Type", "image/jpeg");
//            connection.setRequestProperty("Connection", "close");
//            connection.setRequestProperty("Charset", "UTF-8");

            //连接
            connection.connect();
            //得到响应码
            int responseCode = connection.getResponseCode();
            String responseMessage = connection.getResponseMessage();
            System.out.println("responseMessage:" + responseMessage);
            /**
             * 获取所有的响应头信息
             */
//            Map<String, List<String>> headerFields = connection.getHeaderFields();
//            Set<Map.Entry<String, List<String>>> entries = headerFields.entrySet();
//                        System.out.println("headerFields:"+headerFields);
//
//            for (Map.Entry<String, List<String>> entry : entries) {
//                System.out.println("Key : " + entry.getKey() +" ,Value : " + entry.getValue());
//            }
            //获取响应头信息 Content-disposition
            String headerField = connection.getHeaderField("Content-disposition");
            System.out.println("headerField:" + headerField);
            //从响应头总中提取文件名
            String filename = headerField.substring(headerField.indexOf("\"") + 1, headerField.lastIndexOf("\""));
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //得到响应流
                InputStream inputStream = connection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                //将响应流转换成字符串
                File file = new File(filename);
                FileOutputStream fos = new FileOutputStream(file, true);
                byte[] bytes = new byte[1024];
                int len;
                while ((len = inputStream.read(bytes)) != -1) {
                    fos.write(bytes, 0, len);
                }
                fos.flush();
                // 释放资源
                inputStream.close();
                fos.close();
                connection.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传图文消息内的图片获取URL
     * http请求方式: POST，https协议  以表单形式提交
     * https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN
     */
    @Test
    public void test6() {
        String redisToken = wxService.getRedisToken();
        String urlStr = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN";
        String replaceUrl = urlStr.replace("ACCESS_TOKEN", redisToken);
        File file = new File("/Users/LiuShihao/IdeaProjects/wxthepublic/FtA17e_zIAqlFyfOZ-tpkbhPmZFQ-EIKGef4NwwPfNaPqGARTs4eX7yrX8gAV5Mv.jpg");
        StringBuilder resp = new StringBuilder();
        String result = null;
        try {
            URL urlObj = new URL(replaceUrl);
            HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
            conn.setRequestMethod("POST");//以POST方式提交表单
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", "UTF-8");
            //数据边界
            String boundary = "----------" + System.currentTimeMillis();
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            //获取输出流
            OutputStream out = conn.getOutputStream();
            //创建文件输入流
            FileInputStream fis = new FileInputStream(file);
            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append(boundary);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
            sb.append("Content-Type: applicatin/octet-stream\r\n\r\n");
            out.write(sb.toString().getBytes());

            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
            String foot = "\r\n--" + boundary + "--\r\n";
            out.write(foot.getBytes());
            out.flush();
            out.close();
            //读取数据
            if (HttpsURLConnection.HTTP_OK == conn.getResponseCode()) {

                StringBuffer strbuffer = null;
                BufferedReader reader = null;
                try {
                    strbuffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String lineString = null;
                    while ((lineString = reader.readLine()) != null) {
                        strbuffer.append(lineString);
                    }
                    result = strbuffer.toString();
                } catch (IOException e) {
                    System.out.println("发送POST请求出现异常！" + e);
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        reader.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(result);
        //resp:{"url":"http:\/\/mmbiz.qpic.cn\/mmbiz_jpg\/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibcWic0Kbkq8pgnf4BJgytEOUBxmlIM1IBCPsWLOXUW0dUwAZziaiajwNfw\/0"}
        //http://mmbiz.qpic.cn/mmbiz_jpg/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibcWic0Kbkq8pgnf4BJgytEOUBxmlIM1IBCPsWLOXUW0dUwAZziaiajwNfw/0
    }

    /**
     * 新增其他类型永久素材   https POST  表单
     * 通过POST表单来调用接口，表单id为media，包含需要上传的素材内容，有filename、filelength、content-type等信息
     * https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE
     */
    @Test
    public void test9() {
        String redisToken = wxService.getRedisToken();
        String str = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";
        //新增图片
//        String replace1 = str.replace("ACCESS_TOKEN", redisToken).replace("TYPE", "image");
        //新增语音
//        String replace = str.replace("ACCESS_TOKEN", redisToken).replace("TYPE", "voice");
        //新增缩略图
//        String replace1 = str.replace("ACCESS_TOKEN", redisToken).replace("TYPE", "thumb");
        //新增视频
        String replace1 = str.replace("ACCESS_TOKEN", redisToken).replace("TYPE", "video");
//        File file = new File("/Users/LiuShihao/IdeaProjects/wxthepublic/FtA17e_zIAqlFyfOZ-tpkbhPmZFQ-EIKGef4NwwPfNaPqGARTs4eX7yrX8gAV5Mv.jpg");
        File file = new File("/Users/LiuShihao/Desktop/jay1.jpg");
        StringBuilder resp = new StringBuilder();
        String result = null;
        try {
            URL urlObj = new URL(replace1);
            HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
            conn.setRequestMethod("POST");//以POST方式提交表单
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", "UTF-8");
            //数据边界
            String boundary = "----------" + System.currentTimeMillis();
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            //获取输出流
            OutputStream out = conn.getOutputStream();
            //创建文件输入流
            FileInputStream fis = new FileInputStream(file);
            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append(boundary);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
            sb.append("Content-Type: applicatin/octet-stream\r\n\r\n");
            out.write(sb.toString().getBytes());

            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
            String foot = "\r\n--" + boundary + "--\r\n";
            out.write(foot.getBytes());
            out.flush();
            out.close();
            //读取数据
            if (HttpsURLConnection.HTTP_OK == conn.getResponseCode()) {

                StringBuffer strbuffer = null;
                BufferedReader reader = null;
                try {
                    strbuffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String lineString = null;
                    while ((lineString = reader.readLine()) != null) {
                        strbuffer.append(lineString);
                    }
                    result = strbuffer.toString();
                } catch (IOException e) {
                    System.out.println("发送POST请求出现异常！" + e);
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        reader.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(result);
        //{"media_id":"q27RhD1fN9vtgwdtzGIURdEVO6-Xi1UCqxN3rQMjqXA",
        // "url":"http://mmbiz.qpic.cn/mmbiz_jpg/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibcWic0Kbkq8pgnf4BJgytEOUBxmlIM1IBCPsWLOXUW0dUwAZziaiajwNfw/0?wx_fmt=jpeg",
        // "item":[]}

    }

    public String test12(String path,String title, String introduction) {
        String redisToken = wxService.getRedisToken();
        String str = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE";
        //新增视频
        String replace1 = str.replace("ACCESS_TOKEN", redisToken).replace("TYPE", "video");
        File file = new File(path);
        StringBuilder resp = new StringBuilder();
        String result = null;
        try {
            URL urlObj = new URL(replace1);
            HttpsURLConnection conn = (HttpsURLConnection) urlObj.openConnection();
            conn.setRequestMethod("POST");//以POST方式提交表单
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", "UTF-8");
            //数据边界
            String boundary = "----------" + System.currentTimeMillis();
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            //获取输出流
            OutputStream out = conn.getOutputStream();
            //创建文件输入流
            FileInputStream fis = new FileInputStream(file);
            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append(boundary);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"media\"; filename=\"" + file.getName() + "\"\r\n");
            sb.append("Content-Type: video/mp4\r\n\r\n");
            System.out.println(sb.toString());
            out.write(sb.toString().getBytes());

            byte[] bytes = new byte[1024];
            int len;
            while ((len = fis.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("--");
            sb2.append(boundary);
            sb2.append("\r\n");
            sb2.append("Content-Disposition: form-data;name=\"description\";\r\n\r\n");
            sb2.append("{\"title\":\"" + title + "\",\"introduction\":\"" + introduction + "\"}");
            sb2.append("\r\n--" + boundary + "--\r\n");
            System.out.println(sb2.toString());
            out.write(sb2.toString().getBytes());
            out.flush();
            out.close();
            //读取数据
            if (HttpsURLConnection.HTTP_OK == conn.getResponseCode()) {

                StringBuffer strbuffer = null;
                BufferedReader reader = null;
                try {
                    strbuffer = new StringBuffer();
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    String lineString = null;
                    while ((lineString = reader.readLine()) != null) {
                        strbuffer.append(lineString);
                    }
                    result = strbuffer.toString();
                } catch (IOException e) {
                    System.out.println("发送POST请求出现异常！" + e);
                    e.printStackTrace();
                } finally {
                    if (reader != null) {
                        reader.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(result);
        return result;
    }



    /**
     * 测试上传临时素材
     * @throws Exception
     */
    @Test
    public void test() throws Exception {
//        String path = "/Users/LiuShihao/Desktop/jay1.jpg";
        String path ="/Users/LiuShihao/Downloads/mp4/蒲公英的约定.mp4";
//        String image = uploadTemp(path, "image");
//        String image = uploadTemp(path, "thumb");
//        String image1 = uploadPic(path, "image");
//        System.out.println(image);
        //41005	缺少多媒体文件数据
        //{"type":"image","media_id":"FtA17e_zIAqlFyfOZ-tpkbhPmZFQ-EIKGef4NwwPfNaPqGARTs4eX7yrX8gAV5Mv","created_at":1603115022,"item":[]}
        //{"type":"thumb","thumb_media_id":"gAGEkIl7qY8y8pGWJRQYBQqtl7Nj01bM6j8zpeTQzehDSbIYFteAsMO_9ED3tL2l","created_at":1603208534,"item":[]}
        String title="蒲公英的约定.mp4";
        String introduction="说好要一起旅行，是你如今唯一坚持的任性 ，一起长大的约定，那样真心，与你聊不完的曾经，而我已经分不清，你是友情还是错过的爱情";
        String s = test12(path,title,introduction);
        //{"media_id":"q27RhD1fN9vtgwdtzGIURbGHgecfD731sEEcO1x1wZA","item":[]}

    }
    /**
     * 微信新增永久素材
     * http请求方式：POST
     * 使用https
     * https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN
     * 调用实例：
     * {
     *     "articles": [{
     *      "title": TITLE,
     *     "thumb_media_id": THUMB_MEDIA_ID,
     *     "author": AUTHOR,
     *     "digest": DIGEST,
     *     "show_cover_pic": SHOW_COVER_PIC(0 / 1),
     *     "content": CONTENT,
     *     "content_source_url": CONTENT_SOURCE_URL,
     *     "need_open_comment":1,
     *     "only_fans_can_comment":1
     * },
     *     //若新增的是多图文素材，则此处应还有几段articles结构
     * ]
     * }
     */
    //TODO 新增永久图文素材   不搞
    @Test
    public void test3(){

        String redisToken = wxService.getRedisToken();
        String urlStr = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN";
        String replaceUrl = urlStr.replace("ACCESS_TOKEN", redisToken);
        ArrayList<UploadYJSC> articles = new ArrayList<>();
        //永久素材
        UploadYJSC yjsc = new UploadYJSC();
        //标题
        yjsc.setTitle("yiyangqianxi");
        //图文消息的封面图片素材id（必须是永久mediaID）
        yjsc.setThumb_media_id("q27RhD1fN9vtgwdtzGIURdEVO6-Xi1UCqxN3rQMjqXA");
        //作者
        yjsc.setAuthor("LiuShihao");
        //图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空。如果本字段为没有填写，则默认抓取正文前64个字。
        yjsc.setDigest("");
        //是否显示封面，0为false，即不显示，1为true，即显示
        yjsc.setShow_cover_pic("1");
        //图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS,涉及图片url必须来源 "上传图文消息内的图片获取URL"接口获取。外部图片url将被过滤。
        yjsc.setContent("http://mmbiz.qpic.cn/mmbiz_jpg/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibcWic0Kbkq8pgnf4BJgytEOUBxmlIM1IBCPsWLOXUW0dUwAZziaiajwNfw/0?wx_fmt=jpeg");
        //图文消息的原文地址，即点击“阅读原文”后的URL
        yjsc.setContent_source_url("http://mmbiz.qpic.cn/mmbiz_jpg/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibcWic0Kbkq8pgnf4BJgytEOUBxmlIM1IBCPsWLOXUW0dUwAZziaiajwNfw/0");
        //Uint32 是否打开评论，0不打开，1打开
        yjsc.setNeed_open_comment("1");
        //Uint32 是否粉丝才可评论，0所有人可评论，1粉丝才可评论
        yjsc.setOnly_fans_can_comment("0");
        articles.add(yjsc);
        String jsonString = com.alibaba.fastjson.JSONObject.toJSONString(articles);
        System.out.println(jsonString);
        httpClient(replaceUrl,"POST",jsonString);
    }

    /** 推送音乐消息
     *   缩略图id q27RhD1fN9vtgwdtzGIURT3YONu6lrjoKbmxiKbpKUw
     *   {"media_id":"q27RhD1fN9vtgwdtzGIUReh7gPUiNx6eBNXT-dwv8oU","url":"http:\/\/mmbiz.qpic.cn\/mmbiz_png\/OsaJQib8ibD8ActRfkHSkvZsntjEjmicVGibZoj3n7vOQGGFUVzmZ7PdPwMUTceCHQJANfiaCH5YmNhVjr8oUNnTFWg\/0?wx_fmt=png","item":[]}
     */
    @Test
    public void test10(){
        Music music = new Music();
        music.setTitle("为你封麦");
        music.setDescription("周式失恋废柴情歌 用耍赖打败爱情的失败");
        music.setMusicURL("http://116.62.13.104/group1/M00/00/00/rB52RF-O-b-APHtOAEW_f4JXtVY979.mp3");
        music.setThumbMediaId("q27RhD1fN9vtgwdtzGIUReh7gPUiNx6eBNXT-dwv8oU");
        MusicMessage musicMessage = new MusicMessage();
        musicMessage.setMusic(music);
        XStream stream = new XStream();
        stream.processAnnotations(MusicMessage.class);
        String msgXML = stream.toXML(musicMessage);

    }


}
