package com.wx;

import com.alibaba.fastjson.JSONObject;
import com.wx.model.news.ContentList;
import com.wx.util.HttpUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/19 3:39 下午
 * @desc ：
 */
@SpringBootTest
public class NewsTest {
    @Test
    public void test(){
            String host = "http://ali-news.showapi.com";
            String path = "/newsList";
            String method = "GET";
            String appcode = "3b659fb1d3a0478d9e55621e6f87632b";
            Map<String, String> headers = new HashMap<String, String>();
            //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
            headers.put("Authorization", "APPCODE " + appcode);
            Map<String, String> querys = new HashMap<String, String>();
            //新闻频道id，必须精确匹配  国内焦点
            // querys.put("channelId", "5572a108b3cdc86cf39001cd");
            //娱乐最新
            querys.put("channelId", "5572a10ab3cdc86cf39001eb");
            //新闻频道名称，可模糊匹配
            querys.put("channelName", "");
            //新闻id，可用此信息取得一条新闻记录
            querys.put("id", "");
            //每页最大请求数,默认是20
            querys.put("maxResult", "10");
            //是否需要返回所有的图片及段落属行allList。
            querys.put("needAllList", "0");
            //是否需要返回正文，1为需要，其他为不需要
            querys.put("needContent", "0");
            //是否需要返回正文的html格式，1为需要，其他为不需要
            querys.put("needHtml", "0");
            //页数，默认1。每页最多20条记录。
            querys.put("page", "1");
            //标题名称，可模糊匹配
            querys.put("title", "");
            try {
                /**
                 * 重要提示如下:
                 * HttpUtils请从
                 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
                 * 下载
                 *
                 * 相应的依赖请参照
                 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
                 */
                HttpResponse response = HttpUtils.doGet(host, path, headers, querys);
                System.out.println(response.toString());
                //获取response的body
                HttpEntity entity = response.getEntity();
                String respJsonStr = EntityUtils.toString(response.getEntity());
                System.out.println(respJsonStr);
                //将josn字符串解析成JSONObject
                JSONObject jsonObject = JSONObject.parseObject(respJsonStr);
                System.out.println("jsonObject："+jsonObject);
                //从JSONObject中提取contentlist
                JSONObject body = (JSONObject) jsonObject.get("showapi_res_body");
                JSONObject pagebean = (JSONObject) body.get("pagebean");
                String contentlist = pagebean.get("contentlist").toString();
                System.out.println(pagebean.get("contentlist").toString());
                //解析contentlist为ContentList集合
                List<ContentList> contentLists = JSONObject.parseArray(contentlist, ContentList.class);
                System.out.println("ContentList集合:"+contentLists.size());
                for (ContentList contentList : contentLists) {
                    System.out.println("日期"+contentList.getPubDate());
                    System.out.println("新闻频道"+contentList.getChannelName());
                    System.out.println("图片地址"+contentList.getImg());
                    System.out.println("新闻链接"+contentList.getLink());
                    System.out.println("新闻标题"+contentList.getTitle());
                    System.out.println("新闻媒体"+contentList.getSource());
                    System.out.println("-------------------------------------");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}
