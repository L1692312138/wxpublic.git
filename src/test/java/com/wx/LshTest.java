package com.wx;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Constructor;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/18 10:34 上午
 * @desc ：
 */
@SpringBootTest
public class LshTest {
    @Test
    public void test()  {
        Object o = null;
        try {
            o = test1("java.lang.Long", "1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("O："+o.getClass().getName());

    }

    public Object test1(String className, String value) throws Exception {
        Class<?> clazz = Class.forName(className);
        System.out.println("clazz："+clazz.getName());
        Constructor<?> constructor = clazz.getConstructor(String.class);
        System.out.println("constructor:"+constructor);
        Object o = constructor.newInstance(value);
        return o;
    }

    @Test
    public void test1(){
//        LocalDateTime now = LocalDateTime.now();
//        //当天是几号
//        String todayDD = now.format(DateTimeFormatter.ofPattern("dd"));
//        //当月
//        String todayMM = now.format(DateTimeFormatter.ofPattern("MM"));
//        //下个月
//        String nextMM = now.plusMonths(1).format(DateTimeFormatter.ofPattern("MM"));
//        //当年
//        String todayyyyy = now.format(DateTimeFormatter.ofPattern("yyyy"));
        LocalDateTime yesterday = LocalDateTime.of(2020, 11, 30, 0, 0, 0);//昨天
        LocalDateTime of = LocalDateTime.of(2020, 12, 15, 0, 0, 0);
        Duration between = Duration.between(yesterday, of);
        long l = between.toDays();
        System.out.println(l);

    }
}
