package com.wx;

import com.wx.model.FastDFSFile;
import com.wx.repository.FastDFSFileRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/22 10:11 下午
 * @desc ：
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class FdfsTest {
    @Autowired
    FastDFSFileRepository repository;
    @Test
    public void test(){
        FastDFSFile fastDFSFileByMusicId = repository.findFastDFSFileByMusicId((int) ((Math.random() * 10) + 6));
        System.out.println(fastDFSFileByMusicId);
    }

}
