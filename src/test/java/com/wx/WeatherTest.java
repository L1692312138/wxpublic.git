package com.wx;

import com.wx.model.templatemessage.First;
import com.wx.model.templatemessage.Keyworld;
import com.wx.model.templatemessage.Remark;
import com.wx.model.templatemessage.WxTemplateMessage;
import com.wx.model.weather.WeatherIndex;
import com.wx.model.weather.WeatherResponse;
import com.wx.model.weather.WeatherResult;
import com.wx.service.Impl.WXServiceImpl;
import com.wx.util.BDUtil;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/12/15 10:09 上午
 * @desc ：
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WeatherTest {

    private static final String LSH_ID = "oCV_T6IutImdnwuD658EyIMM_goU";
    private static final String WWJ_ID = "oCV_T6EZuuUohEzn1ZTXCyd_l65I";

    @Autowired
    BDUtil bdUtil;
    @Autowired
    WXServiceImpl  wxService;

    @Test
    public void test1(){
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        // cityid  144  citycode:101091101   city :秦皇岛
        String weather = bdUtil.getWeatherByCitycode("101091101");
        System.out.println(weather);
        WeatherResponse response = com.alibaba.fastjson.JSONObject.parseObject(weather, WeatherResponse.class);
        WeatherResult result = com.alibaba.fastjson.JSONObject.parseObject(response.getResult(), WeatherResult.class);
        ArrayList<Keyworld> keyworlds = new ArrayList<>();
        First first = new First("今天是" + result.getDate() + "，" + result.getWeek() + "", "#173177");
        Keyworld keyworld0 = new Keyworld(result.getCity(), "#FF2D2D"); //红色
        Keyworld keyworld1 = new Keyworld(result.getWeather(), "#FF0080");
        Keyworld keyworld2 = new Keyworld(result.getTemplow() + "℃", "#FF5809");
        Keyworld keyworld3 = new Keyworld(result.getTemphigh() + "℃", "#46A3FF");
        keyworlds.add(keyworld0);
        keyworlds.add(keyworld1);
        keyworlds.add(keyworld2);
        keyworlds.add(keyworld3);
        Remark remark = new Remark("天气转凉，注意添衣保暖，不要感冒了哦^_^", "#173177");
        List<WeatherIndex> index = com.alibaba.fastjson.JSONObject.parseArray(result.getIndex(), WeatherIndex.class);
        StringBuffer sb1 = new StringBuffer();
        for (WeatherIndex weatherIndex : index) {
            Keyworld keyworld11 = new Keyworld(weatherIndex.getIvalue(), "#173177");
            keyworlds.add(keyworld11);
        }
        net.sf.json.JSONObject first1 = net.sf.json.JSONObject.fromObject(first);
        net.sf.json.JSONObject templateData2 = new net.sf.json.JSONObject();
        templateData2.put("first", first1);
        for (int i = 0; i < keyworlds.size(); i++) {
            net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(keyworlds.get(i));
            templateData2.put("keyword" + i, jsonObject);
        }
        templateData2.put("remark", remark);
        WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(WWJ_ID, "amFxIoRc4fCObVXSXZPQf26NZO7xedq_-rrlIRfDApw", "null", null);
        net.sf.json.JSONObject msgData = net.sf.json.JSONObject.fromObject(wxTemplateMessage1);
        msgData.put("data", templateData2.toString());
        System.out.println(msgData);
        String post = httpClient(access_token, "POST", msgData.toString());

    }

    @Test
    public void OpenSchoolRemind(){
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        LocalDateTime openSchoolDay = LocalDateTime.of(2021, 02, 19, 0, 0, 0);
        LocalDateTime now = LocalDateTime.now();
        Duration between = Duration.between(now, openSchoolDay);
        long l = between.toDays();
        if (l<0){
            return;
        }
        String yyyyMMdd = now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        String week = "";
        switch (dayOfWeek){
            case MONDAY:
                week = "星期一";
                break;
            case TUESDAY:
                week = "星期二";
                break;
            case WEDNESDAY:
                week = "星期三";
                break;
            case THURSDAY:
                week = "星期四";
                break;
            case FRIDAY:
                week = "星期五";
                break;
            case SUNDAY:
                week = "星期六";
                break;
            default:
                week = "星期天";
                break;
        }
        First first = new First("王文静同学","#FF8040"); //橙色
        Keyworld keyworld0 = new Keyworld(yyyyMMdd+" "+week,"#173177");//蓝色
        Keyworld keyworld1 = new Keyworld(" 开学 ","#FF2D2D");//红色
        Keyworld keyworld2 = new Keyworld(String.valueOf(l),"#FF2D2D"); //红色
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject templateData = new JSONObject();
        templateData.put("first",first1);
        templateData.put("keyword0",keyword01);
        templateData.put("keyword1",keyword11);
        templateData.put("keyword2",keyword21);
        WxTemplateMessage wxTemplateMessage = new WxTemplateMessage(LSH_ID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null,null);
        JSONObject msgData = JSONObject.fromObject(wxTemplateMessage);
        msgData.put("data",templateData.toString());
        System.out.println(msgData);
        httpClient(access_token, "POST", msgData.toString());
    }


    public String httpUrl(String Url,String RequestMethod){
        StringBuffer sb = null;
        try {
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            httpUrlConn.setRequestMethod(RequestMethod);
            //设置请求头
            httpUrlConn.setRequestProperty("X-Bce-Signature","AppCode/0a7f170702e341f6b3cbd8a4e8274cb9");
            //httpUrlConn.connect();     可要    可不要
            // 获取输入流
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String str = sb.toString();
        return str;

    }

    public String httpClient(String Url,String RequestMethod,String data){
        StringBuffer sb = null;
        try {
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            String s = RequestMethod.toUpperCase();
            httpUrlConn.setRequestMethod(s);
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            if ("GET".equalsIgnoreCase(RequestMethod)){
                httpUrlConn.connect();
            }else {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
                writer.write(data);
                writer.close();
            }
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
        System.out.println(sb.toString());
        return sb.toString();
    }
}
