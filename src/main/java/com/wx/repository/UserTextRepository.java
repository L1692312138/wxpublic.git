package com.wx.repository;

import com.wx.model.UserText;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/13 10:39 上午
 * @desc ：
 */

public interface UserTextRepository extends JpaRepository<UserText,Integer> {
}
