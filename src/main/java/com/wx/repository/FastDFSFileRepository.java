package com.wx.repository;

import com.wx.model.FastDFSFile;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/22 9:50 下午
 * @desc ：
 */
public interface FastDFSFileRepository extends JpaRepository<FastDFSFile,Integer> {

    FastDFSFile findFastDFSFileByMusicId(int id);
    FastDFSFile findFastDFSFileByVideoId(int id);

}
