package com.wx.repository;

import com.wx.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 2:13 下午
 * @desc ：
 */
public interface UserRepository extends JpaRepository<User,Integer> {
    @Query(value = "select * from user where flag = '1' ", nativeQuery = true)
    List<User> findUserByFlag();

    @Query(value = "select * from user where openID = ? and flag = '1'", nativeQuery = true)
    User findByOpenId(String openID);

    @Modifying
    @Query(value = "UPDATE `user` SET `name` = :#{#user.name}}, `tel` = :#{#user.tel}}, `openID` = :#{#user.openID}}, `message` = :#{#user.message}}, `downdate` = :#{#user.downdate}}, `flag` = :#{#user.flag}}, `latitude` = :#{#user.latitude}}, `longitude` = :#{#user.longitude}} WHERE `id` = :#{#user.id}", nativeQuery = true)
    void uploadUserLocaltion(User user);
}
