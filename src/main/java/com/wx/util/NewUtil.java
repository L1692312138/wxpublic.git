package com.wx.util;

import com.alibaba.fastjson.JSONObject;
import com.wx.model.message.Item;
import com.wx.model.news.ContentList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/10 2:21 下午
 * @desc ：查新新闻接口
 */
@Component
public class NewUtil {
    private static final String host = "http://ali-news.showapi.com";
    private static final String path = "/newsList";
    private static final String appcode = "3b659fb1d3a0478d9e55621e6f87632b";
    //娱乐焦点
    private static final String ylchannelId = "5572a10ab3cdc86cf39001eb";
    //国内焦点
    private static final String gnchannelId = "5572a108b3cdc86cf39001cd";
//    private static final String method = "GET";
    public Item[] getNews(String channelId){
        Item[] items = new Item[8];
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        //新闻频道id，必须精确匹配
        querys.put("channelId", channelId);
        //新闻频道名称，可模糊匹配
        querys.put("channelName", "");
        //新闻id，可用此信息取得一条新闻记录
        querys.put("id", "");
        //每页最大请求数,默认是20
        querys.put("maxResult", "8");
        //是否需要返回所有的图片及段落属行allList。
        querys.put("needAllList", "0");
        //是否需要返回正文，1为需要，其他为不需要
        querys.put("needContent", "0");
        //是否需要返回正文的html格式，1为需要，其他为不需要
        querys.put("needHtml", "0");
        //页数，默认1。每页最多20条记录。
        querys.put("page", "1");
        //标题名称，可模糊匹配
        querys.put("title", "");
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doGet(host, path, headers, querys);
            System.out.println(response.toString());
            //获取response的body
            HttpEntity entity = response.getEntity();
            String respJsonStr = EntityUtils.toString(response.getEntity());
            System.out.println(respJsonStr);
            //将josn字符串解析成JSONObject
            JSONObject jsonObject = JSONObject.parseObject(respJsonStr);
            System.out.println("jsonObject："+jsonObject);
            //从JSONObject中提取contentlist
            JSONObject body = (JSONObject) jsonObject.get("showapi_res_body");
            JSONObject pagebean = (JSONObject) body.get("pagebean");
            String contentlist = pagebean.get("contentlist").toString();
            System.out.println(pagebean.get("contentlist").toString());
            //解析contentlist为ContentList集合
            List<ContentList> contentLists = JSONObject.parseArray(contentlist, ContentList.class);
            System.out.println("ContentList集合:"+contentLists.size());
            for (int i = 0; i < contentLists.size() ; i++) {
                ContentList contentList = contentLists.get(i);
                Item item = new Item();
                item.setUrl(contentList.getLink());
                String defaultImgUrl = "";
                defaultImgUrl = contentList.getImg();
                if ("null".equals(defaultImgUrl)||null == defaultImgUrl){
                     defaultImgUrl = "http://mmbiz.qpic.cn/mmbiz_jpg/G6FDfpc5D6nMJnbfic9pTng5kVeKRkQBVfMCbcKcYB6xsfMSekD4EoR2o7gW7nVkCpLhicCGFCu3icG4DDmr2KDGg/0";
                }
                item.setPicUrl(defaultImgUrl);
                item.setTitle(contentList.getTitle());
                item.setDescription(contentList.getTitle());
                items[i] = item;
                System.out.println("日期"+contentList.getPubDate());
                System.out.println("新闻频道"+contentList.getChannelName());
                System.out.println("图片地址"+defaultImgUrl);
                System.out.println("新闻链接"+contentList.getLink());
                System.out.println("新闻标题"+contentList.getTitle());
                System.out.println("新闻媒体"+contentList.getSource());
                System.out.println("-------------------------------------"); 
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }
}
