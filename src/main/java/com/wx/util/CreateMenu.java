package com.wx.util;

import com.wx.model.button.*;
import com.wx.service.Impl.WXServiceImpl;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 12:32 下午
 * @desc ：
 */
@Component
public class CreateMenu {
    @Autowired
    WXServiceImpl wxService;


    public static void main(String[] args) {

    }
    public void createMenu(){
        Button button = new Button();
        button.getButton().add(new ViewButton("博客","https://blog.csdn.net/DreamsArchitects"));
        SubButton subButton1 = new SubButton("电影/音乐");
        subButton1.getSub_button().add(new ClickButten("今日影片","V1001_TODAY_VIDEO"));
        subButton1.getSub_button().add(new ClickButten("今日歌曲","V1002_TODAY_MUSIC"));
        SubButton subButton = new SubButton("新闻资讯");
//        subButton.getSub_button().add(new PhotoAlbumButton("拍照或相册发图","pic_photo_or_album","rselfmenu_1_0"));
        subButton.getSub_button().add(new ClickButten("国内焦点","V1003_TODAY_NEWS"));
        subButton.getSub_button().add(new ClickButten("国际焦点","V1004_TODAY_NEWS"));
        subButton.getSub_button().add(new ClickButten("娱乐焦点","V1006_TODAY_NEWS"));
        subButton.getSub_button().add(new ClickButten("国内最新","V1005_TODAY_NEWS"));
        subButton.getSub_button().add(new ClickButten("国际最新","V1007_TODAY_NEWS"));
//        subButton.getSub_button().add(new ViewButton("我的网盘","http://wangyitong.club:8081/"));
        button.getButton().add(subButton1);
        button.getButton().add(subButton);
        JSONObject jsonObject = JSONObject.fromObject(button);
        System.out.println(jsonObject);
        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
        String token = WXServiceImpl.getToken();
        url = url.replace("ACCESS_TOKEN",token);
        com.alibaba.fastjson.JSONObject post = CommonUtil.httpsRequest(url, "POST", jsonObject.toString());
        System.out.println("post:"+post);
    }

    /**
     * http请求方式：GET https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN
     */
    public void delMenu(){
        String url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
        String token = wxService.getRedisToken();
        url = url.replace("ACCESS_TOKEN",token);
        com.alibaba.fastjson.JSONObject post = CommonUtil.httpsRequest(url, "GET", null);
        System.out.println("post:"+post);

    }

    /**
     * http请求方式: GET（请使用https协议）https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN
     */
    public void findMenu(){
        String url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN";
        String token = wxService.getRedisToken();
        url = url.replace("ACCESS_TOKEN",token);
        com.alibaba.fastjson.JSONObject post = CommonUtil.httpsRequest(url, "GET", null);
        System.out.println("post:"+post);

    }
}
