/**
 * @Description 
 * @Author fengyuepan
 * @Date 2019年4月11日 上午11:01:30
 * @param 
 */
package com.wx.util;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @ClassName MyX509TrustManager
 * @Description 
 * @Author fengyuepan
 * @Date 2019年4月11日 上午11:01:30
 */


public class MyX509TrustManager implements X509TrustManager {
 
	public void checkClientTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
	}
 
	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {
	}
 
	public X509Certificate[] getAcceptedIssuers() {
		return null;
	}
}
