package com.wx.config;

import com.lsh.common.redis.RedisCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/15 9:04 上午
 * @desc ：
 */
@Configuration
public class BeanConfig {
    @Bean
    public RedisCache redisCache(){
        return new RedisCache();
    }
}
