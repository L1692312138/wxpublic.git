package com.wx.service;

import com.wx.model.Param;

import javax.servlet.ServletInputStream;
import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/2 1:12 下午
 * @desc ：
 */
public interface WXService {
    boolean check(Param param);


    Map<String, String> parseRequest(ServletInputStream inputStream);

    String getRespose(Map<String, String> map);
}
