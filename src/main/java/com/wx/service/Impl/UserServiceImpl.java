package com.wx.service.Impl;

import com.wx.model.User;
import com.wx.repository.UserRepository;
import com.wx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 2:14 下午
 * @desc ：
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
