package com.wx.service;

import com.wx.model.User;

import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 2:14 下午
 * @desc ：
 */
public interface UserService {
    List<User> findAll();
}
