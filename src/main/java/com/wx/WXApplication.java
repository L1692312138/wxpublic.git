package com.wx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/9/28 2:54 下午
 * @desc ：
 */
@EnableScheduling
@SpringBootApplication
public class WXApplication {
    public static void main(String[] args) {
        SpringApplication.run(WXApplication.class);
    }
}
