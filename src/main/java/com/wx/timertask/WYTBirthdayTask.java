package com.wx.timertask;

import com.wx.model.templatemessage.First;
import com.wx.model.templatemessage.Keyworld;
import com.wx.model.templatemessage.WxTemplateMessage;
import com.wx.service.Impl.WXServiceImpl;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/14 2:42 下午
 * @desc ：距离王艺曈生日还有     2020 - 12 - 10
 */
@Component
public class WYTBirthdayTask {
    public static final String NAME = "宝贝";
    public static final String MESSAGE = " 宝贝生日 ";
    public static final String LSH_openID = "oCV_T6IutImdnwuD658EyIMM_goU";//刘世豪
    public static final String WYT_openID = "oCV_T6FlZ4U0EEhNlVi51udzQI8U";//王艺曈
    public static final String  cron1="0 30 8 * * ? " ;
    public static final String  cron2="0 * * * * ? " ;

    @Autowired
    WXServiceImpl wxService;
    @Scheduled(cron = cron1)
    public  void  method(){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime of = LocalDateTime.of(now.getYear(), 12, 10, 00, 0, 0);
        Duration between = Duration.between(now, of);
        long l = between.toDays();
        if (l<0){
            between = Duration.between(now, LocalDateTime.of(now.plusYears(1).getYear(), 12, 10, 00, 0, 0));
        }
        l = between.toDays();
        String days = Long.toString(l);
        String redisToken = wxService.getRedisToken();
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";
        String access_token = url.replace("ACCESS_TOKEN", redisToken);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        String week = "";
        switch (dayOfWeek){
            case MONDAY:
                week = "星期一";
                break;
            case TUESDAY:
                week = "星期二";
                break;
            case WEDNESDAY:
                week = "星期三";
                break;
            case THURSDAY:
                week = "星期四";
                break;
            case FRIDAY:
                week = "星期五";
                break;
            case SATURDAY:
                week = "星期六";
                break;
            default:
                week = "星期天";
                break;
        }
        String yyyyMMdd = now.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        First first = new First(NAME,"#FF8040");
        Keyworld keyworld0 = new Keyworld(yyyyMMdd+" "+week,"#173177");
        Keyworld keyworld1 = new Keyworld(MESSAGE,"#173177");
        Keyworld keyworld2 = new Keyworld(days,"#FF2D2D");
        JSONObject first1 = JSONObject.fromObject(first);
        JSONObject keyword01 = JSONObject.fromObject(keyworld0);
        JSONObject keyword11 = JSONObject.fromObject(keyworld1);
        JSONObject keyword21 = JSONObject.fromObject(keyworld2);
        JSONObject templateData = new JSONObject();
        templateData.put("first",first1);
        templateData.put("keyword0",keyword01);
        templateData.put("keyword1",keyword11);
        templateData.put("keyword2",keyword21);
        WxTemplateMessage wxTemplateMessage1 = new WxTemplateMessage(LSH_openID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null,null);
        WxTemplateMessage wxTemplateMessage2 = new WxTemplateMessage(WYT_openID, "hlHap2QQCf-0oddoe-NDmdP0o8-DU6KFzUBwOwVNods", null,null);
        JSONObject msgData1 = JSONObject.fromObject(wxTemplateMessage1);
        JSONObject msgData2 = JSONObject.fromObject(wxTemplateMessage2);
        msgData1.put("data",templateData.toString());
        msgData2.put("data",templateData.toString());
        System.out.println(msgData1);
        httpClient(access_token, "POST", msgData1.toString());
        httpClient(access_token, "POST", msgData2.toString());

    }

    public String httpClient(String Url,String RequestMethod,String data){
        StringBuffer sb = null;
        try {
//            建立连接
            URL url = new URL(Url);
            HttpURLConnection httpUrlConn = (HttpURLConnection) url.openConnection();
            httpUrlConn.setDoInput(true);
            httpUrlConn.setDoOutput(true);
            String s = RequestMethod.toUpperCase();
            httpUrlConn.setRequestMethod(s);
            httpUrlConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            if ("GET".equalsIgnoreCase(RequestMethod)){
                httpUrlConn.connect();
            }else {
                //设置请求头   //设置参数类型是json格式
                //httpUrlConn.connect();     可要    可不要
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpUrlConn.getOutputStream(), "UTF-8"));
                writer.write(data);
                writer.close();
                // 获取输入流

            }
            InputStream inputStream = httpUrlConn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            // 读取返回结果
            sb = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            httpUrlConn.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
        return sb.toString();
    }

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime of = LocalDateTime.of(now.getYear(), 12, 10, 00, 0, 0);
        Duration between = Duration.between(now, of);
        long l = between.toDays();
        if (l<0){
            between = Duration.between(now, LocalDateTime.of(now.plusYears(1).getYear(), 12, 10, 00, 0, 0));
        }
        l = between.toDays();
        System.out.println(l);
    }

}
