package com.wx.model;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/20 2:19 下午
 * @desc ：
 */
@Data
public class UploadYJSC {
    private String title;
    private String thumb_media_id;
    private String author;
    private String digest;
    private String show_cover_pic;
    private String content;
    private String content_source_url;
    private String need_open_comment;
    private String only_fans_can_comment;
}
