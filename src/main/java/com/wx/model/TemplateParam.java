/**
 * @Description 
 * @Author fengyuepan
 * @Date 2019年4月10日 下午6:49:52
 * @param 
 */
package com.wx.model;

import lombok.Data;
import lombok.ToString;

/**
 * @ClassName TemplateParam
 * @Description 
 * @Author fengyuepan
 * @Date 2019年4月10日 下午6:49:52
 */
@Data
@ToString
public class TemplateParam {
	// 参数名称
	private String name;
	// 参数值
	private String value;
	// 颜色
	private String color;
	public TemplateParam(String name, String value, String color) {
		super();
		this.name = name;
		this.value = value;
		this.color = color;
	}
	
   
	
 
}
