package com.wx.model.news;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/19 4:20 下午
 * @desc ：
 */
@Data
public class ShowapiResBody {
    private String ret_code;
    private String pagebean;
}
