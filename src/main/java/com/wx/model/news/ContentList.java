package com.wx.model.news;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/19 4:25 下午
 * @desc ：
 */
@Data
public class ContentList {
    /**
     * 日期
     */
    private  String pubDate;
    /**
     * 新闻频道   国内焦点
     */
    private  String channelName;
    private  String channelId;
    /**
     * 新闻链接
     */
    private  String link;
    private  String img;
    private  String allList;
    /**
     * 新闻标题
     */
    private  String title;
    /**
     * 网易新闻
     */
    private  String source;

}
