package com.wx.model.news;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/19 4:20 下午
 * @desc ：
 */
@Data
public class News {
    private String showapi_res_error;
    private String showapi_res_id;
    private String showapi_res_code;
    private String showapi_res_body;
}
