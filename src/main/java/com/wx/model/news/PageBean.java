package com.wx.model.news;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/19 4:21 下午
 * @desc ：
 */
@Data
public class PageBean {
    private String allPages;
    /**
     * 数组
     */
    private String contentlist;
    private String currentPage;
    private String allNum;
    private String maxResult;
}
