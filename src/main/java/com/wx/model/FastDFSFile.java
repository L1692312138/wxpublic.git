package com.wx.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/22 9:46 下午
 * @desc ：
 */
@Data
@Entity(name = "fastdfsfile")
public class FastDFSFile {
    @Id
    private Integer id;
    private String url;
    private String name;
    private Integer isMusic;
    private Integer musicId;
    private Integer videoId;
    private String title;
    private String description;
}
