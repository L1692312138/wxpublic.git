package com.wx.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2021/9/13 10:36 上午
 * @desc ：
 */
@Data
@Entity(name = "user_text")
public class UserText {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String openID;
    private String userName;
    private String text;
}
