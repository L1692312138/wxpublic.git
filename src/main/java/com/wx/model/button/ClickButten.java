package com.wx.model.button;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 10:39 上午
 * @desc ：
 */
@Data
public class ClickButten extends AbstractButton {
    private String type = "click";
    private String key;

    public ClickButten(String name,String key){
        super(name);
        this.key = key;

    }
}
