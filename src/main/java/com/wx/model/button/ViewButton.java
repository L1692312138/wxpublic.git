package com.wx.model.button;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 10:42 上午
 * @desc ：
 */
@Data
public class ViewButton extends AbstractButton {
    private String type = "view";
    private String url;
    public ViewButton(String name,String url){
        super(name);
        this.url=url;
    }
}
