package com.wx.model.button;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 10:00 上午
 * @desc ：
 */
@Data
public abstract class AbstractButton {
    private String name;

    public AbstractButton(String name){
        this.name = name;
    }
}
