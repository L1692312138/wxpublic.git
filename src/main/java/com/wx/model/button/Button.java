package com.wx.model.button;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 9:57 上午
 * @desc ：
 */
@Data
public class Button {
    private List<AbstractButton> button =  new ArrayList<>();

}
