package com.wx.model.button;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 10:45 上午
 * @desc ：
 */
@Data
public class SubButton extends AbstractButton {
    private List<AbstractButton> sub_button = new ArrayList<>();

    public SubButton(String name ){
        super(name);
    }
}
