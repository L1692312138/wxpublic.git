package com.wx.model.button;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 10:52 上午
 * @desc ：
 *  {
 *             "name": "发图",
 *             "sub_button": [
 *                 {
 *                     "type": "pic_sysphoto",
 *                     "name": "系统拍照发图",
 *                     "key": "rselfmenu_1_0",
 *                    "sub_button": [ ]
 *                  },
 *                 {
 *                     "type": "pic_photo_or_album",
 *                     "name": "拍照或者相册发图",
 *                     "key": "rselfmenu_1_1",
 *                     "sub_button": [ ]
 *                 },
 *                 {
 *                     "type": "pic_weixin",
 *                     "name": "微信相册发图",
 *                     "key": "rselfmenu_1_2",
 *                     "sub_button": [ ]
 *                 }
 *             ]
 *         }
 */
@Data
public class PhotoAlbumButton extends AbstractButton {
    private String type;
    private String key;
    private List<AbstractButton> sub_button = new ArrayList<>();

    public PhotoAlbumButton(String name,String type,String key){
        super(name);
        this.key = key;
        this.type = type;
    }


}
