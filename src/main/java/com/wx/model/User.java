package com.wx.model;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 2:09 下午
 * @desc ：
 */
@Data
@Entity(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String name;
    private String phone;
    private String openID;
    private String message;
    private String downdate;
    private String flag;
    //维度
    private String latitude;
    //经度
    private String longitude;
    private String city;
    private String birthday;
    private String markdate;
}
