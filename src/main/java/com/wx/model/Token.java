package com.wx.model;

import lombok.Data;

@Data
public class Token {
    /**
     * 接口访问凭证
     */
    private String accessToken;
    /**
     * 凭证过期时间  毫秒
     */
    private long expiresTime;

    /**
     *{"access_token":"ACCESS_TOKEN","expires_in":7200}
     * @param accessToken  接口访问凭证
     * @param expiresIn    有效时间         凭证有效时间，单位：秒
     */
    public Token(String accessToken, String expiresIn){
        this.accessToken = accessToken;
        this.expiresTime = System.currentTimeMillis()+Integer.parseInt(expiresIn)*1000;
    }

    /**
     * 判断accessToken是否过期
     * @return    返回true   过期
     */
    public Boolean isExpired(){
        return System.currentTimeMillis()>expiresTime;
    }
}
