package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:14 下午
 * @desc ：
 */
@Data
public class WeatherResponse {
    private String status;
    private String msg;
    //   天气信息
    private String result;

}
