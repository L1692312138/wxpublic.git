package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:28 下午
 * @desc ：一周天气情况
 */
@Data
public class WeatherDaily {
    private String date;
    private String week;
    private String sunrise;
    private String sunset;
    private String night;
    private String day;
}
