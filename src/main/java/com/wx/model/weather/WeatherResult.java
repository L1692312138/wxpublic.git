package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:16 下午
 * @desc ：
 */
@Data
public class WeatherResult {
    private String city;
    private String cityid;
    private String citycode;
    //日期 2020-10-11
    private String date;
    // 星期日
    private String week;
    // 天气情况   多云
    private String weather;
    //最高温度
    private String temphigh;
    //最低温度
    private String templow;
    //静风
    private String winddirect;
    //风力   0级
    private String windpower;
    //更新时间
    private String updatetime;
    //生活指南
    private String index;
    //空气指标
    private String aqi;
    //一周天气情况
    private String daily;
}
