package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:26 下午
 * @desc ：
 *                  "level":"一级",
 *                 "color":"#00e400",
 *                 "affect":"空气质量令人满意，基本无空气污染",
 *                 "measure":"各类人群可正常活动"
 */
@Data
public class WeatherAQIInfo {
    private String level;
    private String color;
    private String affect;
    private String measure;

}
