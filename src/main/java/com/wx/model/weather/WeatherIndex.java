package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:21 下午
 * @desc ：
 */
@Data
public class WeatherIndex {
    //指数类型
    private String iname;
    private String ivalue;
    private String detail;
}
