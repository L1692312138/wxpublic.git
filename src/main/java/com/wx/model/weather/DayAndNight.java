package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:31 下午
 * @desc ：
 */
@Data
public class DayAndNight {
    private String weather;
    private String templow;
    private String temphigh;
    private String winddirect;
    private String windpower;
}
