package com.wx.model.weather;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/11 12:24 下午
 * @desc ：空气指标信息
 */
@Data
public class WeatherAQI {
    //优
    private String quality;
    private String aqiInfo;
}
