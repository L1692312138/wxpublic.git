package com.wx.model.templatemessage;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/12 2:01 下午
 * @desc ：
 */
@Data
public class Miniprogram {
    private String appid;
    private String pagepath;
}
