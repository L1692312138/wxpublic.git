package com.wx.model.templatemessage;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/12 2:56 下午
 * @desc ：
 */
@Data
public class Remark {
    private String value;
    private String color;
    public Remark(String value,String color){
        this.value = value;
        this.color = color;
    }

}
