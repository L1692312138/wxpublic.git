package com.wx.model.templatemessage;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/12 2:55 下午
 * @desc ：订单商品
 */
@Data
public class Keyworld {
    private String value;
    private String color;
    public Keyworld(String value,String color){
        this.value = value;
        this.color = color;
    }
    public Keyworld(){

    }
}
