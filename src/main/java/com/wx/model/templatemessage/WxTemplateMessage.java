package com.wx.model.templatemessage;

import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/12 2:00 下午
 * @desc ：
 */
@Data
public class WxTemplateMessage {
    private String touser;
    private String template_id  ;
    private String url;
    private String miniprogram;
    private String data;

    public WxTemplateMessage(String touser ,String template_id, String url,String data){
      this.touser = touser;
      this.template_id = template_id;
      this.url = url;
      this.data = data;
    }
    public WxTemplateMessage(){

    }
}
