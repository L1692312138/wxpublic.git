package com.wx.model.message;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 1:41 下午
 * @desc ：
 */

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;
@Data
@XStreamAlias("xml")
public class EventMessage extends BaseMessage {

    @XStreamAlias("Event")
    private String event ;
    @XStreamAlias("EventKey")
    private String eventKey ;

    public EventMessage(Map<String ,String> map,String event,String eventKey){
        super(map);
        this.setMsgType("event");
        this.event = event;
        this.eventKey = eventKey;
    }
}
