package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/21 8:43 上午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class LocationMessage extends BaseMessage {
    /**
     * 地理位置纬度
     */
    @XStreamAlias("Location_X")
    private String Location_X;
    /**
     * 地理位置经度
     */
    @XStreamAlias("Location_Y")
    private String Location_Y;
    /**
     * 地图缩放大小
     */
    @XStreamAlias("Scale")
    private String Scale;
    /**
     * 地理位置信息
     */
    @XStreamAlias("Label")
    private String Label;
}
