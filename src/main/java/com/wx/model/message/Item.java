package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/21 8:47 上午
 * @desc ：
 */
@Data
@XStreamAlias("item")
public class Item {
    private String Title;
    private String Description;
    private String PicUrl;
    private String Url;
}
