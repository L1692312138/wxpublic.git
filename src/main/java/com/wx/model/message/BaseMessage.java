package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 3:49 下午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class BaseMessage {
    @XStreamAlias("ToUserName")
    private String toUserName;
    @XStreamAlias("FromUserName")
    private String fromUserName;
    @XStreamAlias("CreateTime")
    private String createTime;
    @XStreamAlias("MsgType")
    private String msgType;
    @XStreamAlias("MsgId")
    private String msgId;


    public BaseMessage(Map<String ,String> map){
        this.fromUserName=map.get("ToUserName");
        this.toUserName=map.get("FromUserName");
        this.createTime = System.currentTimeMillis()/1000+"";
    }
    public BaseMessage(){

    }
}
