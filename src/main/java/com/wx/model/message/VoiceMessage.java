package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 2:51 下午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class VoiceMessage extends BaseMessage {
    //MediaId	语音消息媒体id，可以调用获取临时素材接口拉取数据。
    @XStreamAlias("MediaId")
    private String mediaId;
    //Format	语音格式，如amr，speex等
    @XStreamAlias("Format")
    private String format;
    //Recognition	语音识别结果，UTF8编码
    @XStreamAlias("Recognition")
    private String recognition;
    public VoiceMessage(Map<String ,String> map){
        super(map);
        this.setMsgType("voice");
    }

}
