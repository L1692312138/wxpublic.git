package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/9 3:50 下午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class TextMessage extends BaseMessage {
    @XStreamAlias("Content")
    private String content;

    public TextMessage(Map<String ,String> map,String content ){
        super(map);
        this.setMsgType("text");
        this.content=content;
    }

}
