package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/21 8:49 上午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class VideoMessage  extends BaseMessage  {
    @XStreamAlias("Video")
    private Video Video;
    public VideoMessage(Map<String ,String> map, Video video){
        super(map);
        this.setMsgType("video");
        this.Video = video;

    }
}
