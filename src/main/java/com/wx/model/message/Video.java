package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/21 8:46 上午
 * @desc ：
 */
@Data
public class Video {
    @XStreamAlias("Title")
    private String Title;
    @XStreamAlias("Description")
    private String Description;
    @XStreamAlias("MediaId")
    private String MediaId;
}
