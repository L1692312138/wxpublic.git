package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/10 2:49 下午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class ImageMessage extends BaseMessage {
    //PicUrl	图片链接（由系统生成）
    @XStreamAlias("PicUrl")
    private String picUrl;
    //MediaId	图片消息媒体id，可以调用获取临时素材接口拉取数据。
    @XStreamAlias("MediaId")
    private String mediaId;

    public ImageMessage(Map<String ,String> map){
        super(map);
        this.setMsgType("image");
    }


}
