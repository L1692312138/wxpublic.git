package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/10 2:06 下午
 * @desc ：
 */
@Data
@XStreamAlias("Articles")
public class Articles {

    private Item[] item;
}
