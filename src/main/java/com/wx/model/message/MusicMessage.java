package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/10/16 4:47 下午
 * @desc ：
 * http://url.cn/https://c.y.qq.com/base/fcgi-bin/u?__=zJGAHE9
 * http://url.cn/https://c.y.qq.com/base/fcgi-bin/u?__=zJGAHE9
 */
@Data
@XStreamAlias("xml")
public class MusicMessage extends BaseMessage{
    @XStreamAlias("Music")
    private Music music;

    public MusicMessage(Map<String ,String> map,Music music){
        super(map);
        this.setMsgType("music");
        this.music = music;
    }

    public MusicMessage(){

    }
}
