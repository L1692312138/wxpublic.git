package com.wx.model.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * @author ：LiuShihao
 * @date ：Created in 2020/11/10 2:05 下午
 * @desc ：
 */
@Data
@XStreamAlias("xml")
public class NewsMessage extends BaseMessage {
    /**
     * 图文消息信息，注意，如果图文数超过限制，则将只发限制内的条数
     */
    @XStreamAlias("Articles")
    private Item[] articles;
    /**
     * 图文消息个数；
     * 当用户发送文本、图片、语音、视频、图文、地理位置这六种消息时，开发者只能回复1条图文消息；
     * 其余场景最多可回复8条图文消息
     */
    @XStreamAlias("ArticleCount")
    private int ArticleCount;
    public NewsMessage(Map<String ,String> map, Item[] item){
        super(map);
        this.setMsgType("news");
        this.articles = item;
    }

    public NewsMessage(){

    }
}
